// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/fb.h>
#include <linux/module.h>
#include <linux/pci.h>
#ifdef CONFIG_X86
#include <linux/screen_info.h>
#endif
#include <linux/version.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(6, 0, 0)
#include <linux/aperture.h>
#endif

MODULE_AUTHOR("Gerd Hoffmann <kraxel@redhat.com>");
MODULE_DESCRIPTION("Bochs DISPI Interface Framebuffer Driver");
MODULE_VERSION("2.1.0");
MODULE_LICENSE("GPL");

static char *mode;
module_param(mode, charp, 0);
MODULE_PARM_DESC(mode, "Set bochsfb resolution, default is 1024x768");

#define BOCHSFB_FB_RES			0
#define BOCHSFB_DISPI_RES		2

#define BOCHSFB_DISPI_IO_SIZE		2
#define BOCHSFB_DISPI_IO_INDEX		0x01ce
#define BOCHSFB_DISPI_IO_DATA		0x01cf

#define BOCHSFB_DISPI_ID		0x00
#define BOCHSFB_DISPI_XRES		0x01
#define BOCHSFB_DISPI_YRES		0x02
#define BOCHSFB_DISPI_BPP		0x03
#define BOCHSFB_DISPI_ENABLE		0x04
#define BOCHSFB_DISPI_BANK		0x05
#define BOCHSFB_DISPI_VIRT_WIDTH	0x06
#define BOCHSFB_DISPI_VIRT_HEIGHT	0x07
#define BOCHSFB_DISPI_X_OFFSET		0x08
#define BOCHSFB_DISPI_Y_OFFSET		0x09

#define BOCHSFB_DISPI_DISABLED		0x00
#define BOCHSFB_DISPI_ENABLED		0x01
#define BOCHSFB_DISPI_LFB_ENABLED	0x40

enum {
	BOCHSFB_STDVGA,
	BOCHSFB_SIMICS,
	BOCHSFB_VBOXVGA
};

struct bochsfb_dev {
	void __iomem *dispi;
};

#ifdef CONFIG_X86
static bool bochsfb_ioports;
#endif

static __always_inline void bochsfb_vga_attr_set(struct bochsfb_dev *bochsfb, u8 attr)
{
	if (bochsfb->dispi) {
		writeb(0x01, bochsfb->dispi + 0x0402); /* Enable color mode */
		(void)readb(bochsfb->dispi + 0x041a); /* Reset flip-flop */
		writeb(attr, bochsfb->dispi + 0x0400);
#ifdef CONFIG_X86
	} else if (bochsfb_ioports) {
		outb(0x01, 0x03c2); /* Enable color mode */
		(void)inb(0x03da); /* Reset flip-flop */
		outb(attr, 0x03c0);
#endif
	}
}

static __always_inline u16 bochsfb_ioread(struct bochsfb_dev *bochsfb, u16 port)
{
	if (bochsfb->dispi) {
		u32 reg = 0x0500 + ((u32)port << 1);

		return readw(bochsfb->dispi + reg);
#ifdef CONFIG_X86
	} else if (bochsfb_ioports) {
		outw(port, BOCHSFB_DISPI_IO_INDEX);
		return inw(BOCHSFB_DISPI_IO_DATA);
#endif
	} else {
		return 0xffff;
	}
}

static __always_inline void bochsfb_iowrite(struct bochsfb_dev *bochsfb, u16 port, u16 val)
{
	if (bochsfb->dispi) {
		u32 reg = 0x0500 + ((u32)port << 1);

		writew(val, bochsfb->dispi + reg);
#ifdef CONFIG_X86
	} else if (bochsfb_ioports) {
		outw(port, BOCHSFB_DISPI_IO_INDEX);
		outw(val, BOCHSFB_DISPI_IO_DATA);
#endif
	}
}

static const char * const bochsfb_id[] = {
	"Bochs VGA", "Simics VGA", "VBox VGA", NULL
};

static const struct fb_fix_screeninfo bochsfb_fix = {
	.id		= KBUILD_MODNAME,
	.type		= FB_TYPE_PACKED_PIXELS,
	.visual		= FB_VISUAL_TRUECOLOR,
	.accel		= FB_ACCEL_NONE,
	.xpanstep	= 1,
	.ypanstep	= 1,
};

static const struct fb_var_screeninfo bochsfb_var = {
	.xres		= 1024,
	.yres		= 768,
	.bits_per_pixel	= 32,
	.transp.offset	= 24,
	.red.offset	= 16,
	.green.offset	= 8,
	.blue.offset	= 0,
	.transp.length	= 8,
	.red.length	= 8,
	.green.length	= 8,
	.blue.length	= 8,
	.vmode		= FB_VMODE_NONINTERLACED,
	.width		= -1,
	.height		= -1,
	.left_margin	= 16,
	.right_margin	= 16,
	.upper_margin	= 16,
	.lower_margin	= 16,
	.hsync_len	= 8,
	.vsync_len	= 8,
	.pixclock	= 10000,
};

static int bochsfb_check_var(struct fb_var_screeninfo *var, struct fb_info *info)
{
	u32 pixels, vx, vy;

	if (var->xres > 0xffff || var->xres_virtual > 0xffff ||
	    var->bits_per_pixel != 32 || (var->vmode & FB_VMODE_MASK) != FB_VMODE_NONINTERLACED)
		return -EINVAL;

	vx = var->xres_virtual;
	if (vx < var->xres)
		vx = var->xres;

	pixels = info->fix.smem_len * 8 / var->bits_per_pixel;
	vy = pixels / vx;
	if (vy < var->yres)
		return -EINVAL;

	var->xres_virtual = vx;
	var->yres_virtual = vy;
	var->xoffset = 0;
	var->yoffset = 0;
	return 0;
}

static int bochsfb_set_par(struct fb_info *info)
{
	struct bochsfb_dev *bochsfb = info->par;

	info->fix.line_length = info->var.xres * info->var.bits_per_pixel / 8;

	bochsfb_vga_attr_set(bochsfb, 0x20);

	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_ENABLE, BOCHSFB_DISPI_DISABLED);
	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_BANK, 0);
	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_X_OFFSET, 0);
	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_Y_OFFSET, 0);
	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_BPP, info->var.bits_per_pixel);
	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_XRES, info->var.xres);
	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_YRES, info->var.yres);
	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_VIRT_WIDTH, info->var.xres_virtual);
	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_VIRT_HEIGHT, info->var.yres_virtual);
	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_ENABLE,
				 BOCHSFB_DISPI_ENABLED | BOCHSFB_DISPI_LFB_ENABLED);
	return 0;
}

static int bochsfb_setcolreg(unsigned regno, unsigned red, unsigned green, unsigned blue,
			     unsigned __always_unused transp, struct fb_info *info)
{
	if (regno >= 16 || info->var.bits_per_pixel != 32 ||
	    info->fix.visual != FB_VISUAL_TRUECOLOR)
		return -EINVAL;

	red >>= 8;
	green >>= 8;
	blue >>= 8;

	((u32 *)(info->pseudo_palette))[regno] = (red << info->var.red.offset) |
						 (green << info->var.green.offset) |
						 (blue << info->var.blue.offset);
	return 0;
}

static int bochsfb_pan_display(struct fb_var_screeninfo *var, struct fb_info *info)
{
	struct bochsfb_dev *bochsfb = info->par;

	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_X_OFFSET, var->xoffset);
	bochsfb_iowrite(bochsfb, BOCHSFB_DISPI_Y_OFFSET, var->yoffset);
	return 0;
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 6, 0)
static struct fb_ops bochsfb_ops = {
#else
static const struct fb_ops bochsfb_ops = {
#endif
	.owner		= THIS_MODULE,
	.fb_check_var	= bochsfb_check_var,
	.fb_set_par	= bochsfb_set_par,
	.fb_setcolreg	= bochsfb_setcolreg,
	.fb_pan_display	= bochsfb_pan_display,
	.fb_fillrect	= cfb_fillrect,
	.fb_copyarea	= cfb_copyarea,
	.fb_imageblit	= cfb_imageblit,
};

static void bochsfb_remove_conflicting_devices(unsigned long base, unsigned long size)
{
#if LINUX_VERSION_CODE < KERNEL_VERSION(6, 0, 0)
	struct apertures_struct *ap;

	ap = alloc_apertures(1);
	if (!ap)
		return;
	ap->ranges[0].base = base;
	ap->ranges[0].size = size;
	remove_conflicting_framebuffers(ap, KBUILD_MODNAME, false);
	kfree(ap);
#elif (LINUX_VERSION_CODE >= KERNEL_VERSION(6, 0, 0) &&		\
	LINUX_VERSION_CODE < KERNEL_VERSION(6, 5, 0))
	aperture_remove_conflicting_devices(base, size, false, KBUILD_MODNAME);
#else
	aperture_remove_conflicting_devices(base, size, KBUILD_MODNAME);
#endif
}

static int bochsfb_check_mode(struct fb_var_screeninfo *var, u32 width, u32 height,
			      unsigned long size)
{
	u32 xres = clamp_val(width,  320, 2560);
	u32 yres = clamp_val(height, 200, 1600);

	if (xres * yres * 4 > size)
		return -EINVAL;
	var->xres = xres;
	var->yres = yres;
	return 0;
}

static int bochsfb_pci_probe(struct pci_dev *pdev, const struct pci_device_id *pci_id)
{
	struct fb_info *info;
	struct bochsfb_dev *bochsfb;
	unsigned long base, size;
	u32 xres, yres;
	u16 dispi_id;
	int ret;

	info = framebuffer_alloc(sizeof(struct bochsfb_dev), &pdev->dev);
	if (!info) {
		dev_err(&pdev->dev, "Cannot allocate framebuffer structure\n");
		return -ENOMEM;
	}
	bochsfb = info->par;

	ret = pci_enable_device(pdev);
	if (ret) {
		dev_err(&pdev->dev, "Cannot enable PCI device\n");
		goto free_fb_info;
	}

	/* DISPI PCI BAR QEMU 1.3+ */
	if ((pci_id->driver_data != BOCHSFB_VBOXVGA) &&
	    (pci_resource_flags(pdev, BOCHSFB_DISPI_RES) & IORESOURCE_MEM)) {
		ret = pci_request_region(pdev, BOCHSFB_DISPI_RES, KBUILD_MODNAME);
		if (ret) {
			dev_err(&pdev->dev, "Cannot request DISPI region\n");
			goto free_pci_device;
		}

		bochsfb->dispi = pci_ioremap_bar(pdev, BOCHSFB_DISPI_RES);
		if (!bochsfb->dispi) {
			dev_err(&pdev->dev, "Cannot map the DISPI region\n");
			ret = -ENOMEM;
			goto free_pci_regions;
		}
		dev_info(&pdev->dev, "DISPI region at %pR\n", &pdev->resource[BOCHSFB_DISPI_RES]);
	} else {
#ifdef CONFIG_X86
		/* DISPI IO Ports */
		struct resource *res;

		if (bochsfb_ioports) {
			dev_err(&pdev->dev, "Cannot request DISPI IO region\n");
			ret = -EBUSY;
			goto free_pci_device;
		}

		res = request_region(BOCHSFB_DISPI_IO_INDEX, BOCHSFB_DISPI_IO_SIZE, KBUILD_MODNAME);
		if (!res) {
			dev_err(&pdev->dev, "Cannot request DISPI IO region\n");
			ret = -EBUSY;
			goto free_pci_device;
		}
		bochsfb_ioports = true;
		dev_info(&pdev->dev, "DISPI region at %pR\n", res);
#else
		dev_err(&pdev->dev, "Invalid DISPI region\n");
		ret = -ENODEV;
		goto free_pci_device;
#endif
	}

	dispi_id = bochsfb_ioread(bochsfb, BOCHSFB_DISPI_ID);
	if ((dispi_id & 0xfff0) != 0xb0c0) {
		dev_err(&pdev->dev, "Invalid DISPI ID 0x%x\n", dispi_id);
		ret = -ENODEV;
		goto free_dispi;
	}
	dev_info(&pdev->dev, "%s DISPI, ID 0x%x\n", bochsfb_id[pci_id->driver_data], dispi_id);

	if (!(pci_resource_flags(pdev, BOCHSFB_FB_RES) & IORESOURCE_MEM)) {
		dev_err(&pdev->dev, "Framebuffer PCI region not found\n");
		ret = -ENODEV;
		goto free_dispi;
	}

	base = pci_resource_start(pdev, BOCHSFB_FB_RES);
	size = pci_resource_len(pdev, BOCHSFB_FB_RES);
	if (size < SZ_4M) {
		dev_err(&pdev->dev, "Less than 4 MiB of VRAM, ignoring device\n");
		ret = -ENOMEM;
		goto free_dispi;
	}

	bochsfb_remove_conflicting_devices(base, size);

	if (pci_request_region(pdev, BOCHSFB_FB_RES, KBUILD_MODNAME))
		dev_warn(&pdev->dev, "Request LFB region failed\n");

	info->screen_base = ioremap(base, size);
	if (!info->screen_base) {
		dev_err(&pdev->dev, "Cannot map the LFB\n");
		ret = -ENOMEM;
		goto free_dispi;
	}
	fb_memset(info->screen_base, 0, size);

	dev_info(&pdev->dev, "LFB size %lu KiB at %pR\n",
		 size / SZ_1K, &pdev->resource[BOCHSFB_FB_RES]);

	info->fbops = &bochsfb_ops;
	info->flags = FBINFO_HWACCEL_XPAN | FBINFO_HWACCEL_YPAN | FBINFO_READS_FAST;
	info->pseudo_palette = kmalloc_array(16, sizeof(u32), GFP_KERNEL);
	if (!info->pseudo_palette) {
		dev_err(&pdev->dev, "Cannot allocate palette\n");
		ret = -ENOMEM;
		goto free_screen_base;
	}
	info->fix = bochsfb_fix;
	info->fix.smem_start = base;
	info->fix.smem_len = size;
	info->var = bochsfb_var;

#ifdef CONFIG_X86
	if (screen_info.orig_video_isVGA == VIDEO_TYPE_VLFB ||
	    screen_info.orig_video_isVGA == VIDEO_TYPE_EFI) {
		if (!bochsfb_check_mode(&info->var, screen_info.lfb_width,
					screen_info.lfb_height, size)) {
			fb_info(info, "Picking up default mode: %ux%u from %s\n",
				screen_info.lfb_width, screen_info.lfb_height,
				screen_info.orig_video_isVGA == VIDEO_TYPE_VLFB ? "vesa" : "efi");
		}
		screen_info.orig_video_isVGA = 0;
	}
#endif

	if (mode) {
		if (sscanf(mode, "%ux%u", &xres, &yres) == 2) {
			if (bochsfb_check_mode(&info->var, xres, yres, size))
				fb_warn(info, "Unsupported mode requested: %ux%u\n", xres, yres);
		}
	}

	if (bochsfb_check_var(&info->var, info))
		info->var = bochsfb_var;

	ret = register_framebuffer(info);
	if (ret) {
		dev_err(&pdev->dev, "Failed to register framebuffer\n");
		goto free_pseudo_palette;
	}
	pci_set_drvdata(pdev, info);
	return 0;

free_pseudo_palette:
	kfree(info->pseudo_palette);
free_screen_base:
	iounmap(info->screen_base);
free_dispi:
	if (bochsfb->dispi)
		iounmap(bochsfb->dispi);
#ifdef CONFIG_X86
	if (bochsfb_ioports)
		release_region(BOCHSFB_DISPI_IO_INDEX, BOCHSFB_DISPI_IO_SIZE);
#endif
free_pci_regions:
	pci_release_regions(pdev);
free_pci_device:
	pci_disable_device(pdev);
free_fb_info:
	framebuffer_release(info);
	return ret;
}

static void bochsfb_pci_remove(struct pci_dev *pdev)
{
	struct fb_info *info = pci_get_drvdata(pdev);
	struct bochsfb_dev *bochsfb = info->par;

	unregister_framebuffer(info);
	if (bochsfb->dispi)
		iounmap(bochsfb->dispi);
#ifdef CONFIG_X86
	if (bochsfb_ioports)
		release_region(BOCHSFB_DISPI_IO_INDEX, BOCHSFB_DISPI_IO_SIZE);
#endif
	iounmap(info->screen_base);
	pci_release_regions(pdev);
	pci_disable_device(pdev);
	kfree(info->pseudo_palette);
	framebuffer_release(info);
}

static const struct pci_device_id bochsfb_pci_tbl[] = {
	{ PCI_DEVICE(0x1234, 0x1111), .driver_data = BOCHSFB_STDVGA  },
	{ PCI_DEVICE(0x4321, 0x1111), .driver_data = BOCHSFB_SIMICS  },
	{ PCI_DEVICE(0x80ee, 0xbeef), .driver_data = BOCHSFB_VBOXVGA },
	{ }
};
MODULE_DEVICE_TABLE(pci, bochsfb_pci_tbl);

static struct pci_driver bochsfb_pci_driver = {
	.name     = KBUILD_MODNAME,
	.id_table = bochsfb_pci_tbl,
	.probe    = bochsfb_pci_probe,
	.remove   = bochsfb_pci_remove,
};

module_pci_driver(bochsfb_pci_driver);
