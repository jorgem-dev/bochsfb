## Bochs Framebuffer Driver for Linux 4.4+

The driver was taken from [`this patch`](https://lore.kernel.org/patchwork/patch/404210/) from Gerd Hoffmann.

### Installing the bochsfb Module

As root, blacklist `bochs-drm` (Linux < 5.15), `bochs` (Linux >= 5.15) or `vboxvideo` and rebuild `initramfs`:

```
# echo "blacklist bochs" > /etc/modprobe.d/bochs-drm.conf
```

Disable built-in fb drivers ([`efifb`](https://www.kernel.org/doc/html/latest/fb/efifb.html), `offb`, [`vesafb`](https://www.kernel.org/doc/html/latest/fb/vesafb.html), ...) in `/etc/default/grub` and update `grub.cfg`:

```
GRUB_CMDLINE_LINUX_DEFAULT="video=efifb:off"
```

Under ./src run:

```
$ make
$ sudo make install
```
